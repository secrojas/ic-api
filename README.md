<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>



## Set up the  Laravel API

This is an api made with laravel that provides a crud of Tasks.

Instructions:

- clone the repo.
- create local database, use the name <b>ic-api</b> just like in the .env.example file.
- go inside de repo path and run: <b>composer install</b>.
- inside the project <b>copy the .env.example</b> file and <b>rename</b> as <b>.env</b>.
- in the .env file <b>set-up your DB Credentials</b> (database name, user and password).
- run the migrations: <b>php artisan migrate</b>.
- run the seeds: <b>php artisan db:seed --class="TasksTableSeeder"</b>.
- run <b>php artisan serve</b>.

Production

There is a production API for this project, it runs on <b>https://ic-challenge.srojasweb.dev/</b> and the example to access de API would be <b>https://ic-challenge.srojasweb.dev/api/v1</b>.
You can use that for the react app env file.


<?php

use App\Http\Controllers\Api\v1\TaskController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    Route::apiResource('tasks', TaskController::class);

    Route::group(['prefix'=>'tasks','as'=>'tasks'], function () {
        Route::get('done', [TaskController::class, 'listCompleted']);
        Route::put('completed/{id}', [TaskController::class, 'setCompleted']);
    });
});

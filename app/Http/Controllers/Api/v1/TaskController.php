<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Resources\v1\TaskCollection;
use App\Http\Resources\v1\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        if ($request->keyword) {
            return new TaskCollection(Task::where('name', 'LIKE', '%'.$request->keyword.'%')->get());
        } if ($request->completed) {
            return new TaskCollection(Task::where('completed', 1)->get());
        } else {
            return  new TaskCollection(Task::all());
        }
    }

    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    public function store(StoreTaskRequest $request)
    {
        $task = Task::create($request->validated());

        return response()->json($task, 201);
    }

    public function update(StoreTaskRequest $request, Task $task)
    {
        $task->update($request->validated());
        return response()->json($task, 200);
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return 204;
    }

    public function listCompleted()
    {
        return new TaskCollection(Task::where('completed', 1)->get());
    }

    public function setCompleted(int $id)
    {
        $task = Task::findOrFail($id);
        $task->completed = 1;
        $task->save();

        return response()->json($task, 200);
    }
}
